using DelimitedFiles
using FEATools

frequencies = readdlm("elmerfem/frequencies.txt", ' ')[:]
write_paraview_time_array(frequencies, "elmerfem/", "frequency")

