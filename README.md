# Home Studio 6

ElmerFEM model for the acoustic field inside a realistic room, treated with increasing degrees of complexity.

Part of the [accompanying repositories](https://gitlab.com/computational-acoustics) of the [Computational Acoustics with Open Source Software](https://computational-acoustics.gitlab.io/website/) project.

## Covering Episodes

* [Home Studio - Part 4](https://computational-acoustics.gitlab.io/website/posts/22-home-studio-part-4/).

## Study Summary

The main parameters of the study are reported below.

### Source and Medium

|Parameter Name               | Symbol       | Value                                                                        | Unit                      |
|-----------------------------|--------------|------------------------------------------------------------------------------|---------------------------|
| Source Radius               | $`a`$        | 0.005                                                                        | meters                    |
| Source Frequencies          | $`f`$        | from 1 Hz to 125 Hz, in steps of 1 Hz, plus few more frequencies of interest | Hertz                     |
| Source Surface Velocity     | $`U`$        | 10                                                                           | meters per second         |
| Medium Sound Phase Speed    | $`c_{0}`$    | 343                                                                          | meters per second         |
| Medium Equilibrium Density  | $`\rho_{0}`$ | 1.205                                                                        | kilograms per cubic meter |


### Domain

| Shape          | Size                      | Mesh Algorithm  | Mesh Min. Size | Mesh Max. Size   | Element Order       |
|----------------|---------------------------|-----------------|----------------|------------------|---------------------|
| Realistic Room | 4.35 X 2.79 X 2.25 meters | NETGEN 1D-2D-3D | 1 millimetre   | 300 millimetres  | Second (p-elements) |

### Boundary Conditions

| Boundary | Material     |Impedance [MKS Rayls]   |
|----------|--------------|------------------------|
| Walls    | Concrete     | $`8.00 \cdot 10^{6}`$  |
| Ceiling  | Concrete     | $`8.00 \cdot 10^{6}`$  |
| Floor    | Pine         | $`1.57 \cdot 10^{6}`$  |
| Door     | Oak          | $`2.90 \cdot 10^{6}`$  |
| Window   | Silica Glass | $`13.00 \cdot 10^{6}`$ |

Sources:

* [Acoustic Properties of Solids, Onda Corporation, April 11 2003](https://www.ondacorp.com/wp-content/uploads/2020/09/Solids.pdf).
* [Acoustic Properties Tables Abbreviations, Onda Corporation, August 21 2003](https://www.ondacorp.com/wp-content/uploads/2020/09/Abbreviations.pdf).

## Software Overview

The table below reports the software used for this project.

| Software                                           | Usage                        |
|----------------------------------------------------|------------------------------|
| [FreeCAD](https://www.freecadweb.org/)             | 3D Modeller                  |
| [Salome Platform](https://www.salome-platform.org) | Pre-processing               |
| [ElmerFEM](http://www.elmerfem.org)                | Multiphysical solver         |
| [ParaView](https://www.paraview.org/)              | Post-processing              |
| [Julia](https://julialang.org/)                    | Technical Computing Language |

## Repo Structure

* `elmerfem` contains the ElmreFEM project.
* `geometry.FCStd` is the FreeCAD geometry model. This file can be used to export geometric entities to _BREP_ files to pre-process with Salome. _BREP_ files are excluded from the repo as they are redundant.
* `meshing.hdf` is the Salome study of the geometry. It contains the geometry pre-processing and meshing. Note that the mesh in this file is **not** computed.
* `salomeToElmer` is a submodule containing a script to export a Salome mesh to an ElmerFEM mesh, to be used within Salome.
* The `*.pvsm` files are ParaView state files. They can be loaded into ParaView to plot the solution.
* The `frequencies_to_paraview.jl` file contains Julia code to create a ParaView array of frequency values. To run this code, the [FEATools](https://gitlab.com/computational-acoustics/featools.jl) package is necessary.
* The `make_response.jl` file contains Julia code to postprocess probe data exported from ParaView.

The repo contains only the _source_ of the simulation. To obtain results, the study must be solved.

## How to Run this Study

Follow the steps below to run the study.

Clone the repository and `cd` into the cloned directory:

```bash
git clone https://gitlab.com/computational-acoustics/home-studio-6.git
cd home-studio-6/
```

`cd` in the `elmerfem` directory and run the study:

```bash
cd elmerfem/
ElmerSolver
```

When finished, open ParaView and select `File > Load State` to load the `.pvsm` files in the root of the repository. To load the ParaView state successfully, choose _Search files under specified directory_ as shown in the example below. The folder specified in `Data Directory` should be the root folder of the repo (blurred below as the absolute path will differ in your machine).

![Load State Example](res/pictures/paraview-load-state.png)

To run `make_response.jl` without modification, the data from the `Probe Location` filters in the ParaView state need to be exported. For each `Probe Location` filter:

* Select the `Probe Location` object in the `Pipeline Browser`.
* Choose `File > Save Data`.
* Navigate inside the `elmerfem` folder in the repository root directory and use `probe#` as a `File name`, where `#` is the number of the `Probe Location` filer, for example `probe1`. Click `OK`.
* Use `30` as `Precision`, tick `Write Time Steps` and leave the default `File name suffix`. Click `OK`.

At thsi point `make_response.jl` should run:

```bash
cd ..
julia make_response.jl
```


